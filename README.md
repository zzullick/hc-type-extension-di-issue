# HotChocolate 11.0.9 Type Extension DI Issue Bug Repro
This is a repository to illustrate an issue with extensions to query/mutation objects not properly able to resolve scoped DI dependencies from the service collection. 

1. Run the project
2. Navigate to BCP @ https://localhost:5001/graphql
2. Execute the following query:
```
{
  hello(name: "Bob")
  goodbye(name: "Bob")
}
```
3. Observe the error from the operation coming from the extension class vs. the successful operation coming from the root query class.