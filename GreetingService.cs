namespace HCTypeExtensionDIIssue
{
    public interface IGreetingService
    {
        string Hello(string name);
        string Goodbye(string name);
    }

    public class GreetingService : IGreetingService
    {
        public string Hello(string name) => $"Hello, {name}";
        public string Goodbye(string name) => $"Goodbye, {name}";
    }
}