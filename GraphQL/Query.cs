using HotChocolate.Types;

namespace HCTypeExtensionDIIssue
{
    public class Query
    {
        private readonly IGreetingService greetingService;

        public Query(IGreetingService greetingService)
        {
            this.greetingService = greetingService;
        }

        public string Hello(string name)
        {
            return greetingService.Hello(name);
        }
    }

    public class QueryType : ObjectType<Query>
    {
         
    }
}