using HotChocolate.Types;

namespace HCTypeExtensionDIIssue
{
    [ExtendObjectType(nameof(Query))]
    public class QueryExtension
    {
        private readonly IGreetingService greetingService;

        public QueryExtension(IGreetingService greetingService)
        {
            this.greetingService = greetingService;
        }

        public string Goodbye(string name)
        {
            return greetingService.Goodbye(name);
        }
    }
}